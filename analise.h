#ifndef ANALISE_H
#define ANALISE_H

/* ANALISE ALGORITMOS DE ORDENACAO E PESQUISA EM VETOR */

/* TAD - analise */
//Estrutura auxiliar, usada para contar o numero de comparacoes e/ou trocas utilizados
//por algum algoritmo

typedef struct {
    int trocas, comps;
} analise;

/* Funcoes */
analise* initAnalise();

void freeAnalise(analise *alg);

void aumentaTroca(analise *alg);
void aumentaComp(analise *alg);

/* Getters And Setter */
//Acredito que seja uma boa pratica em C?, principalmente para Structs ou Unios mais complexas

void setTrocas(analise *alg, int i);
void setComps(analise *alg, int i);

int getTrocas(analise *alg);
int getComps(analise *alg);

#endif

