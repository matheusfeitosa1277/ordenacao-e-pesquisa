#include <stdlib.h>
#include "analise.h"

/* Implementacao do TAD analise */
analise* initAnalise(){
    analise *alg;

    alg = malloc(sizeof(analise));
    alg->trocas = 0; alg->comps = 0;

    return alg;
}

/* Funcoes */
void freeAnalise(analise *alg){ free(alg); }

void aumentaTroca(analise *alg){ alg->trocas++; }

void aumentaComp(analise *alg){ alg->comps++; }

/* Getters And Setter */ 
void setTrocas(analise *alg, int i){ alg->trocas = i; };

void setComps(analise *alg, int i){ alg->comps = i; };

int getTrocas(analise *alg){ return alg->trocas; }
int getComps(analise *alg){ return alg->comps; }

