#include <stdio.h>
#include <stdlib.h>

/* Funcoes Auxiliares */

/* FACILITADORES PARA VETORES INTEIROS */
//Aloca e retorna um vetor de tam interios aleatorios entre min e max
int* geraVetorRand(int seed, int min, int max, int tam){
    int i, *vetor;

    srand(seed);
    vetor = malloc(sizeof(int)*tam);

    for (i = 0; i < tam; i++)
        vetor[i] = rand() % (max + 1) + min;
    return vetor;
}

//Aloca e retorna um vetor de [ini..(ini+tam-1)]
int* geraVetorCres(int ini, int tam){
    int i, *vetor;

    vetor = malloc(sizeof(int)*tam);

    for (i = 0; i < tam; i++){
        vetor[i] = ini;
        ini++;
    }

    return vetor;
}

//Aloca e retorna um vetor de [(ini+tam-1)->ini]
int* geraVetorDecres(int ini, int tam){
    int i, *vetor;

    vetor = malloc(sizeof(int)*tam);

    for (i = 0; i < tam; i++){
        vetor[i] = ini;
        ini--;
    }

    return vetor;
}

//Copias os primeiros tam elementos de buffer em vetor
void copiaVetor(int *vetor, int *buffer, int tam){
    int i;
    
    for (i = 0; i < tam; i++)
        buffer[i] = vetor[i];

}

//Copias os elementos entre esq e dir do buffer para o vetor, incluindo os mesmos
void copiaSubVetor(int *vetor, int *buffer, int esq, int dir){
    int i;
    
    for (i = esq; i <= dir; i++)
        buffer[i] = vetor[i];
}

//Imprime os elementos entre esq e dir do vetor, incluindo os mesmos
void imprimeVetor(int *vetor, int esq, int dir){
    int i;

    for (i = esq; i <= dir; i++)
        printf("%d ", vetor[i]);

    putchar('\n');
}

//Checa se o subvetor vetor[esq..dir] esta ordenado
int checaOrdenacao(int *vetor, int esq, int dir){
    int i;

    for (i = esq; i < dir; i++)
        if (vetor[i] > vetor[i+1]) return 0;
    return 1;
}

