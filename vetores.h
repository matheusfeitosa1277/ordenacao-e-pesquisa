#ifndef VETORES_H
#define VETORES_H

/* FACILITADORES PARA VETORES DE INTEIROS */

//Retorna um vetor alocado dinamicamente e preenchido com numeros
//pseudo aleatorios entre 0 e max
int* geraVetorRand(int seed, int min, int max, int tam);

//
int* geraVetorCres(int ini, int tam);
int* geraVetorDecres(int ini, int tam);

//
void copiaVetor(int *vetor, int *buffer, int tam);

//Dispensavel com aritmetica de ponterios, facilitador
void copiaSubVetor(int *vetor, int *buffer, int esq, int dir);

//Imprime um vetor de inteiros 
void imprimeVetor(int *vetor, int esq, int dir);

//Verifica se o vetor esta ordenado
//Se for o caso, retorna 1
//Caso contrario, retorna 0
int checaOrdenacao(int *vetor, int esq, int dir);

#endif

