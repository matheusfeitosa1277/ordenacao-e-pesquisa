#ifndef PESQUISA_H
#define PESQUISA_H
#include "analise.h"

/* PESQUISA EM VETOR */

/* Pesquisa Sequencial - Linear Search */
//Procura pela chave, sequencialmente, entre os elementos do indice esq -> dir em um vetor de inteiros
//Se encontrar, retornar o indice da chave 
//Caso contrario, retorna esq-1


//Sem Sentinela
int pesqSeq(int chave, int *vetor, int esq, int dir, analise *alg);

//Com Sentinela na Extremidade Esquerda ou Direita
//Obs: SENTE percorre dir -> esq e retorna dir+1 se nao encontrar
int pesqSeqSENTE(int chave, int *vetor, int esq, int dir, analise *alg);

int pesqSeqSENTD(int chave, int *vetor, int esq, int dir, analise *alg);


/* Pesquisa Binaria - Binary Search */
//Pesquisa pela chave em um vetor ORDENADO de inteiros, entre os indices esq e dir
//Se encontrar, retorna o indice da chave
//Caso contrario, retorna -1

//Recursiva
int pesqBin(int chave, int *vetor, int esq, int dir, analise *alg);

//Interativa
int pesqBinIter(int chave, int *vetor, int esq, int dir, analise *alg);

#endif

