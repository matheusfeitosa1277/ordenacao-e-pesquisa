#include "ordenacao.h"
#include "pesquisa.h"
#include "analise.h"
#include "vetores.h"

#include <stdlib.h>
#include <stdio.h>

#define SEED 42 
#define MAX 450
#define MIN 0
#define TAM 30 + 2 
#define ESQ 1     //Espaco para Sentinela a esquerda
#define DIR TAM-2 //Espaco para Sentinela a direita
#define NSORT 3   //Numero de Algoritmos de Ordenacao 
#define NPESQ 4   //Numero de Algoritmos de Pesquisa
#define NCHAVE 2  //Numero de chaves pesquisadas

/*
     S            S
    [0][1..DIR][TAM-1]
*/

//Perfumaria, para Imprimir
const char* SEARCH[] = {"Seq", "SeqSENTE", "BinRec", "PesqBinIter"};
const char* SORT[] = {"Bubble", "Selection", "Insertion", "Shell", "QuickRec"};

int main(){

    int *randVet, *cresVet, *decresVet, *buffer, i, j;
    analise *alg = initAnalise();
    void (*sortPointer[NSORT])(int*, int, int, analise*) = {bubbleSort, selectionSort, insertionSort, };
                                                           // shellSort, quickSortRec};

    //Inicializando os vetores
    randVet = geraVetorRand(SEED, MIN, MAX, TAM);
    cresVet = geraVetorCres(0, TAM);
    decresVet = geraVetorDecres(TAM-1, TAM);


    puts("Ordenacao Em Vetor");
    if (TAM <= 33) {imprimeVetor(randVet, ESQ, DIR); putchar('\n');} //Imprime em 1 Linha

    //Ordenacao
    buffer = malloc(sizeof(int)*(TAM));   
    for (j = 0; j < NSORT; j++){
        copiaVetor(randVet, buffer, TAM);
        setTrocas(alg, 0); setComps(alg, 0); 

        (*sortPointer[j])(buffer, ESQ, DIR, alg);
        //imprimeVetor(buffer, ESQ, DIR);
        if (checaOrdenacao(buffer, ESQ, DIR))
            printf("%-11s - Trocas: %3d Comp: %3d\n", SORT[j], getTrocas(alg), getComps(alg));
        else printf("ERRO NA ORDENACAO %s\n", SORT[j]);
    } putchar('\n'); 

    //Pesquisa
    int ind, CHAVE[] = {420, 967};
    int (*pesqPointer[NPESQ])(int, int*, int, int, analise*) = {pesqSeq, pesqSeqSENTE, pesqBin, pesqBinIter};

    puts("Pesquisa Em Vetor Ordenado");
    if (TAM <= 33) {imprimeVetor(randVet, ESQ, DIR); putchar('\n');} //Imprime em 1 Linha

    for (i = 0; i < NPESQ; i++){
        printf("%-11s  | Chave | Indice | Comp | Elemento |\n", SEARCH[i]); //Algoritmo de Pesquisa
        for (j = 0; j < NCHAVE; j++) {
            setComps(alg, 0);
            ind = (*pesqPointer[i])(CHAVE[j], buffer, ESQ, DIR, alg);
            printf("%20d | %6d | %4d | %8d |\n",
                   CHAVE[j], ind, getComps(alg),buffer[ind]);

        } putchar('\n'); 
    } putchar('\n');

    free(randVet); free(cresVet); free(decresVet); free(buffer);
    freeAnalise(alg);

    return 0; 
}

