#ifndef ORDENACAO_H
#define ORDENACAO_H

#include "analise.h"

/* ORDENACAO EM VETOR */

/* Funcoes Auxiliares - Usadas pelos algoritmos de ordenacao */

void trocaVetor(int *a, int *b);

//Selection Sort
int minVetor(int *vetor, int esq, int dir, analise *alg);

//Insertion Sort
void arrastaVetor(int *vetor, int esq, int dir, analise *alg);
int pesqSeqInsertion(int *vetor, int esq, int dir, analise *alg);


/* ORDENACAO EM VETOR */

//Bubble Sort - Max | Ok
void bubbleSort(int *vetor, int esq, int dir, analise *alg);
void bubbleSortOpt(int *vetor, int esq, int dir, analise *alg);

//Selection Sort - Min | Ok
void selectionSort(int *vetor, int esq, int dir, analise *alg);

//Insertion Sort - Min | ok
void insertionSort(int *vetor, int esq, int dir, analise *alg);

//Shell Sort - Knuths Sequence | Vale a pena revisar
void shellSort(int *vetor, int esq, int dir, analise *alg); // <- Falta Analise

void quickSortRec(int *vetor, int esq, int dir, analise *alg);

void mergeSortRec(int *vetor, int esq, int dir); // <- Falta Implementar

void heapSort(int *vetor, int esq, int dir);

/* CASOS ESPECIAIS - ORDENACAO LINEAR */

void countingSort(int *vetor, int esq, int dir);

void radixSort(int *vetor, int esq, int dir);

#endif

