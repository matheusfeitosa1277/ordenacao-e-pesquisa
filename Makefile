#Regras de Ligacao
main: main.o pesquisa.o ordenacao.o vetores.o analise.o
	gcc main.o pesquisa.o ordenacao.o vetores.o analise.o -Wall

#Regras de Compilacao
main.o: main.c pesquisa.h ordenacao.h vetores.h analise.h pesquisa.o
	gcc -c main.c -Wall

pesquisa.o: pesquisa.c pesquisa.h analise.h
	gcc -c pesquisa.c -Wall

ordenacao.o: ordenacao.c ordenacao.h analise.h
	gcc -c ordenacao.c -Wall

analise.o: analise.c analise.h
	gcc -c analise.c -Wall

vetores.o: vetores.c vetores.h
	gcc -c vetores.c -Wall

clean:
	rm -f *.o

purge: clean
	rm -f a.out
    
