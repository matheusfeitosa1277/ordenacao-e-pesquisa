#include "analise.h"
#include "pesquisa.h"

/* PESQUISA EM VETOR */

/* Pesquisa Sequencial */
//Sem Sentinela
int pesqSeq(int chave, int *vetor, int esq, int dir, analise *alg){

    do {
        aumentaComp(alg);
        if (vetor[esq] == chave) break;
        esq++;
    } while (esq <= dir);

    return esq;
}

//Com Sentinela na Extremidade Esquerda
#define SENTE  (esq-1)
int pesqSeqSENTE(int chave, int *vetor, int esq, int dir, analise *alg){
    int aux;

    aux = vetor[SENTE];
    vetor[SENTE] = chave;

    loop:
        aumentaComp(alg);
        if (vetor[dir] == chave) goto exit;  //"Break"
        dir--;
        goto loop;
    exit: //while(1) ate sair pelo break

    vetor[SENTE] = aux;
    return dir;
}

//Com Sentinela na Extremidade Direita
#define SENTD  (dir+1)
int pesqSeqSENTD(int chave, int *vetor, int esq, int dir, analise *alg){
    int aux;

    aux = vetor[SENTD];
    vetor[SENTD] = chave;

    loop:
        aumentaComp(alg);
        if (vetor[esq] == chave) goto exit;
        esq++;
        goto loop;
    exit:

    vetor[SENTD] = aux;
    return esq;
}


/* Pesquisa Binaria - Vetor Ordenado */
//Recursiva
int pesqBin(int chave, int *vetor, int esq, int dir, analise *alg){
    int meio, vMeio;

    if (esq > dir) return -1; 

    meio = (esq + dir)/2;
    vMeio = vetor[meio];

    aumentaComp(alg);
    if (vMeio == chave) return meio;

    aumentaComp(alg);
    if (vMeio > chave)
        return pesqBin(chave, vetor, esq, meio - 1, alg); //Elemento a esquerda
    return pesqBin(chave, vetor, meio + 1, dir, alg);     //Elemento a direita
}

//Iterativa
int pesqBinIter(int chave, int *vetor, int esq, int dir, analise *alg){
    int meio, vMeio;

    do {
        meio = (esq + dir)/2;
        vMeio = vetor[meio];

        aumentaComp(alg);
        if (vMeio == chave) return meio;

        aumentaComp(alg);
        if (vetor[meio] > chave) dir = meio - 1; //Elemento a esquerda
        else esq = meio + 1;                     //Elemento a direita

    } while (esq <= dir); 

    return -1; 
}

