#include "ordenacao.h"
#include "vetores.h"
#include "analise.h"

#include <stdio.h>

/* Funcoes Auxiliares  - Usadas por mais de 1 algoritmo de Ordenacao */

//Troca os elementos dentro dos enderecos a e b | aka swap
void trocaVetor(int *a, int *b){
    int aux = *a;
    *a = *b; *b = aux;
}

/* ORDENACAO EM VETOR */

/* ----------------- */
/* Bubble Sort - MAX */
/* ----------------- */

void bubbleSort(int *vetor, int esq, int dir, analise *alg){
    int i;
    
    for (dir = dir;  dir >= esq; dir--){
        for (i = esq; i < dir; i++) {
            aumentaComp(alg);
            if (vetor[i] > vetor[i+1]){
                aumentaTroca(alg);
                trocaVetor(&vetor[i], &vetor[i+1]);
            }
        } //Maior elemento esta ordenado
    }
}

void bubbleSortOpt(int *vetor, int esq, int dir, analise *alg){
    int i, troca;
    
    do {
        troca = 0;
        for (i = esq; i < dir; i++) {
            aumentaComp(alg);
            if (vetor[i] > vetor[i+1]){
                aumentaTroca(alg);
                trocaVetor(&vetor[i], &vetor[i+1]);
                troca = 1;
            }
        } //Maior elemento esta ordenado
        dir--;
    } while (troca);
}

/* -------------------- */
/* Selection Sort - MIN */
/* -------------------- */

//Retorna o indice do menor elemeno do vetor
int minVetor(int *vetor, int esq, int dir, analise *alg){
    int i, min;

    min = esq;
    for (i = esq+1; i <= dir; i++){
        aumentaComp(alg);
        if (vetor[i] < vetor[min]) min = i;
    }

    return min;
}

void selectionSort(int *vetor, int esq, int dir, analise *alg){
    int i, min;
   
    for (i = esq; i <= dir; i++) { 
        min = minVetor(vetor, i, dir, alg);
        aumentaTroca(alg);
        trocaVetor(&vetor[i], &vetor[min]); //Menor elemento esta ordenado
    }
}

/* -------------------- */
/* Insertion Sort - Min */
/* -------------------- */

//"Arrasta" os elementos 1 posicao para direita
//Abrindo espaco para inserir o elemento ordenado
void arrastaVetor(int *vetor, int esq, int dir, analise *alg){
    int i;

    for (i = dir; i > esq; i--){
        aumentaTroca(alg);
        vetor[i] = vetor[i-1];
    }
}

//Encontra, da esquerda para direita, a posicao do novo elemento no vetor
int pesqSeqInsertion(int *vetor, int esq, int dir, analise *alg){ //Busca Sequencial

    loop: 
        aumentaComp(alg);
        if (vetor[dir] <= vetor[esq]) goto exit;
        esq++;
        goto loop;
    exit:

    return esq;
}

void insertionSort(int *vetor, int esq, int dir, analise *alg){
    int i, j, aux;

    for (i = esq; i <= dir; i++) {
        aux = vetor[i];
        j = pesqSeqInsertion(vetor, esq, i, alg); //Posicao a ser inserido
        arrastaVetor(vetor, j, i, alg);           //Libera espaco para inserir
        vetor[j] = aux;
    }
}

/* ---------------------------- */
/* Shell Sort - Knuths Sequence */
/* ---------------------------- */

void shellSort(int *vetor, int esq, int dir, analise *alg){
    int i, j, aux, esp = 1; //Espacamento

    //Knuth's sequence
    do esp = esp*3 + 1; while (esp < dir-esq + 1);

    do {
        esp /= 3;
        for (i = esp + esq; i <= dir; i++) { 
            aux = vetor[i]; j = i;
            while (vetor[j-esp] > aux){
                vetor[j] = vetor[j-esp]; j-=esp;
                if (j < esp+esq) break;
            }
            vetor[j] = aux;
        }

    } while (esp > 1); //Insertion Sort
}

/* -------------------- */
/* Quick Sort Recursivo */
/* -------------------- */

//Funcoes auxiliares
int mediana(int *vetor, int esq, int dir, analise *alg){ //Usando Xor
    int meio = (esq+dir)/2;

   if ((vetor[esq] > vetor[meio]) ^ (vetor[esq] > vetor[dir])) //Maior q um e menor que o outro
       return esq;
   if ((vetor[meio] < vetor[esq]) ^ (vetor[meio] < vetor[dir]))
       return meio;
   return dir;
}

//Particona usando o 1 elemento como pivo
int particiona(int *vetor, int esq, int dir, analise *alg){ //Intuitivo, mas ineficiente
    int pivo, i, j, m;

    i = esq; j = dir;
    m = mediana(vetor, esq, dir, alg);
    trocaVetor(&vetor[esq], &vetor[m]); //Mediana como 1 elemento

    pivo = vetor[esq];
    while (i<=j) {
        while (i <= dir && pivo >= vetor[i]) i++;
        while (i <= dir && pivo  < vetor[j]) j--;
        if (i<j) trocaVetor(&vetor[i], &vetor[j]);
    }
    trocaVetor(&vetor[esq], &vetor[j]); //Pivo na posicao ordenada
    return j;

}

void quickSortRec(int *vetor, int esq, int dir, analise *alg){
    int pivo;

    if (esq >= dir) return; //Caso base
    
    pivo = particiona(vetor, esq, dir, alg);
    quickSortRec(vetor, esq, pivo-1, alg);
    quickSortRec(vetor, pivo+1, dir, alg);
}

//Remover recursao com pilha foge do escopo da disciplina
//void quickSortIter(int *vetor, int esq, int dir){}

/* ---------- */
/* Merge Sort */
/* ---------- */

void mergeSortRec(int *vetor, int esq, int dir){

}

//void mergeSortIter(int *vetor, int esq, int dir){}

/* --------- */
/* Heap Sort */
/* --------- */

void heapfy(int *vetor, int esq, int dir){

}

void heapSort(int *vetor, int esq, int dir){

}

/* ------------- */
/* Counting Sort */
/* ------------- */


/* ---------- */
/* Radix Sort */
/* ---------- */


