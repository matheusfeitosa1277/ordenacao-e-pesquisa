# Algoritmos e estruturas de dados 2

![OrdenacaoPesquisa](https://www.inf.ufpr.br/msf21/resources/ordenacaoPesquisa.png)
 
## Programa - Parte 1 - Ordenação e Pesquisa

-   Introdução aos Algoritmos
    
-   Pesquisa Sequencial & Pesquisa Binária
    
-   Recursividade
    
-   Introdução à Análise de Custo de Algoritmos
    
-   Ordenação: SelectSort, BubbleSort, QuickSort, InsertSort, ShellSort, MergeSort
    
-   Outros algoritmos de ordenação
    
## Resumo

Neste repositório segue a implementação, em C, dos algoritmos de busca e ordenação mencionados acima, bem como uma estrutura para analisar o custo computacional dos mesmos em termos de inspeções e trocas entre elementos
